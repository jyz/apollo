# Architecture

## Modules

apollo is structured in several parts dealing with different concerns of a cluster.

## Infrastructure

[More](providers.md)

## Platform

### Metrics

[More](spaces.md)

### Logs

[More](federation.md)

### Network

[More](network.md)

### Management

[More](management.md)

### Core

[More](core.md)

### Engine

[More](engine.md)

### Orchestrator

[More](orchestrator.md)

### Data

[More](data.md)

### Apps

[More](apps.md)