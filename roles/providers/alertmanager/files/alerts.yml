groups:
  - name: apollo-orchestrator-swarm-nodes
    rules:
    - alert: cpu-high
      expr: 100 - (avg(irate(node_cpu_seconds_total{mode="idle"}[1m]) * ON(instance) GROUP_LEFT(node_name)
        node_meta * 100) BY (node_name, instance)) > 50
      for: 1m
      labels:
        severity: warning
      annotations:
        description: Swarm node {{ $labels.instance }} CPU usage is at {{ humanize
          $value}}%.
        summary: CPU alert for Swarm node '{{ $labels.instance }}'
    - alert: memory-high
      expr: sum(((node_memory_MemTotal_bytes - node_memory_MemAvailable_bytes) / node_memory_MemTotal_bytes)
        * ON(instance) GROUP_LEFT(node_name) node_meta * 100) BY (node_name, instance) > 80
      for: 1m
      labels:
        severity: warning
      annotations:
        description: Swarm node {{ $labels.instance }} memory usage is at {{ humanize
          $value}}%.
        summary: Memory alert for Swarm node '{{ $labels.instance }}'
    - alert: disk-disk
      expr: ((node_filesystem_size_bytes{mountpoint="/"} - node_filesystem_free_bytes{mountpoint="/"})
        * 100 / node_filesystem_size_bytes{mountpoint="/"}) * ON(instance) GROUP_LEFT(node_name,instance)
        node_meta > 85
      for: 1m
      labels:
        severity: warning
      annotations:
        description: Swarm node {{ $labels.instance }} disk usage is at {{ humanize
          $value}}%.
        summary: Disk alert for Swarm node '{{ $labels.instance }}'
    - alert: disk-fill-rate
      expr: predict_linear(node_filesystem_free_bytes{mountpoint="/"}[1h], 6 * 3600) * ON(instance)
        GROUP_LEFT(node_name) node_meta < 0
      for: 1h
      labels:
        severity: critical
      annotations:
        description: Swarm node {{ $labels.instance }} disk is going to fill up in
          6h.
        summary: Disk fill alert for Swarm node '{{ $labels.instance }}'

  - name: apollo-orchestrator-swarm-tasks
    rules:
    - alert: cpu-high
      expr: sum(rate(container_cpu_usage_seconds_total{container_label_com_docker_swarm_task_name=~".+"}[1m]))
        BY (container_label_com_docker_swarm_task_name, instance)
        * 100 > 50
      for: 1m
      annotations:
        description: '{{ $labels.container_label_com_docker_swarm_task_name }} on ''{{
          $labels.instance }}'' CPU usage is at {{ humanize
          $value}}%.'
        summary: CPU alert for Swarm task '{{ $labels.container_label_com_docker_swarm_task_name }}' on '{{ $labels.instance }}'
    - alert: memory-high
      expr: sum(container_memory_rss{container_label_com_docker_swarm_task_name=~".+"})
        BY (container_label_com_docker_swarm_task_name, instance) > 1e+09
      for: 1m
      annotations:
        description: '{{ $labels.container_label_com_docker_swarm_task_name }} on ''{{
          $labels.instance }}'' memory usage is {{ humanize
          $value}}.'
        summary: Memory alert for Swarm task '{{ $labels.container_label_com_docker_swarm_task_name }}' on '{{ $labels.instance }}'